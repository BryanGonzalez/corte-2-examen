let dllAmexicano = 19.85;
let dllAcan = 1.35;
let dllAeuro = 0.99;

//let subTotal =0;
//let comision =0;
let total =0;

let totalDeTotales = 0;
let contadorDeRegistros = 0;

function change() {

    let origen = document.getElementById("monedaOrigen").value;

    const cambio = document.getElementById('monedaDestino');


    if (origen == "Dólar estadounidense"){
        cambio.options[0].style.display= 'contents';
        cambio.options[1].style.display = 'contents';
        cambio.options[2].style.display = 'none';
        cambio.options[3].style.display = 'contents';
        cambio.options[4].style.display = 'contents';
        } else if (origen == "Dolar canadiense"){
            cambio.options[0].style.display = 'contents';
            cambio.options[1].style.display = 'contents';
            cambio.options[2].style.display = 'contents';
            cambio.options[3].style.display = 'none';
            cambio.options[4].style.display = 'contents';
            } else if (origen == "Euro"){
                cambio.options[0].style.display = 'contents';
                cambio.options[1].style.display = 'contents';
                cambio.options[2].style.display = 'contents';
                cambio.options[3].style.display = 'contents';
                cambio.options[4].style.display = 'none';
                } else if (origen == "Peso mexicano"){
                    cambio.options[0].style.display = 'contents';
                    cambio.options[1].style.display = 'none';
                    cambio.options[2].style.display = 'contents';
                    cambio.options[3].style.display = 'contents';
                    cambio.options[4].style.display = 'contents';
                }
}

function Calcular(){

    let lblsubtotal = document.getElementById('subTotal');
    let lblcomision = document.getElementById('totalComision');
    let lbltotal = document.getElementById('totalPagar');
    let cantidad = document.getElementById("cantidad").value;
    let imprimir = document.getElementById("resul");
    let origen = document.getElementById("monedaOrigen").value;
    let destino = document.getElementById("monedaDestino").value;

    var aggSubtotal = 0;
    var aggComision = 0;
    var aggTotal = 0;


     if (cantidad > 0){

        if (origen == "Dólar estadounidense" && destino == "MXN"){
            //Conversion de dollar a peso mexicano
            aggSubtotal = cantidad * dllAmexicano;
            aggComision = aggSubtotal * 0.30;

            imprimir.innerHTML = imprimir.innerHTML + cantidad + " Dolares Estadounidenses a Peso Mexicano: $" + aggSubtotal + "<br>";
            aggTotal = aggSubtotal + aggComision;
        } else if (origen == "Dólar estadounidense" && destino == "CAN"){
                //Conversion de dollar a dolar canadiense
                aggSubtotal = cantidad * dllAcan;
                aggComision = aggSubtotal * 0.30;

                imprimir.innerHTML = imprimir.innerHTML + cantidad + " Dolares Estadounidenses a Dólares Canadiense: $" + aggSubtotal + "<br>";
                aggTotal = aggSubtotal + aggComision;
            } else if (origen == "Dólar estadounidense" && destino == "EU"){
                    //Conversion de dollar a euros
                    aggSubtotal = cantidad * dllAeuro;
                    aggComision = aggSubtotal * 0.30;

                    imprimir.innerHTML = imprimir.innerHTML + cantidad + " Dolares Estadounidenses a Euros: $" + aggSubtotal + "<br>";
                    aggTotal = aggSubtotal + aggComision;
                } else if (origen == "Peso mexicano" && destino == "DLL"){
                        //Conversion de pesos mexicanos a dollares americanos
                        aggSubtotal = cantidad/dllAmexicano;
                        aggComision = aggSubtotal * 0.30;

                        imprimir.innerHTML = imprimir.innerHTML + cantidad + " Pesos Mexicanos a Dólares Estadounidenses: $" + aggSubtotal + "<br>";
                        aggTotal = aggSubtotal + aggComision;
                    } else if (origen == "Peso mexicano" && destino == "CAN"){
                            //Conversion de pesos mexicanos a dollares canadienses
                            let muletilla = cantidad/dllAmexicano;
                            aggSubtotal = muletilla * dllAcan;
                            aggComision = aggSubtotal * 0.30;

                            imprimir.innerHTML = imprimir.innerHTML + cantidad + " Pesos Mexicanos a Dólares Candienses: $" + aggSubtotal + "<br>";
                            aggTotal = aggSubtotal + aggComision;
                        } else if (origen == "Peso mexicano" && destino == "EU"){
                                //Conversion de pesos mexicanos a euros
                                let muletilla = cantidad/dllAmexicano;
                                aggSubtotal = muletilla * dllAeuro;
                                aggComision = aggSubtotal * 0.30;

                                imprimir.innerHTML = imprimir.innerHTML + cantidad + " Pesos Mexicanos a Euros: $" + aggSubtotal + "<br>";
                                aggTotal = aggSubtotal + aggComision;
                            } else if (origen == "Dolar canadiense" && destino == "DLL"){
                                    //Conversion de dolar canadiense a dolar americano
                                    aggSubtotal = cantidad / dllAcan;
                                    aggComision = aggSubtotal * 0.30;
                        
                                    imprimir.innerHTML = imprimir.innerHTML + cantidad + " Dólares Canadiense a Dólares Estadounidenses: $" + aggSubtotal + "<br>";
                                    aggTotal = aggSubtotal + aggComision;
                                } else if (origen == "Dolar canadiense" && destino == "MXN"){
                                        //Conversion de dolar canadiense a peso mexicano
                                        let muletilla  = cantidad / dllAcan;
                                        aggSubtotal = muletilla * dllAmexicano;
                                        aggComision = aggSubtotal * 0.30;
                            
                                        imprimir.innerHTML = imprimir.innerHTML + cantidad + " Dólares Canadiense a Dólares Estadounidenses: $" + aggSubtotal + "<br>";
                                        aggTotal = aggSubtotal + aggComision;
                                    } else if (origen == "Dolar canadiense" && destino == "EU"){
                                            //Conversion de dolar canadiense a euro
                                            let muletilla  = cantidad / dllAcan;
                                            aggSubtotal = muletilla * dllAeuro;
                                            aggComision = aggSubtotal * 0.30;
                                
                                            imprimir.innerHTML = imprimir.innerHTML + cantidad + " Dólares Canadiense a Dólares Estadounidenses: $" + aggSubtotal + "<br>";
                                            aggTotal = aggSubtotal + aggComision;
                                        } else if (origen == "Euro" && destino == "DLL"){
                                                //Conversion de euro a dolar americano
                                                aggSubtotal = cantidad / dllAeuro;
                                                aggComision = aggSubtotal * 0.30;
                                    
                                                imprimir.innerHTML = imprimir.innerHTML + cantidad + " Euros a Dólares Estadounidenses: $" + aggSubtotal + "<br>";
                                                aggTotal = aggSubtotal + aggComision;
                                            } else if (origen == "Euro" && destino == "MXN"){
                                                    //Conversion de euro a peso mexicano
                                                    let muletilla = cantidad / dllAeuro;
                                                    aggSubtotal = muletilla * dllAmexicano;
                                                    aggComision = aggSubtotal * 0.30;
                                        
                                                    imprimir.innerHTML = imprimir.innerHTML + cantidad + " Euros a Pesos Mexicanos: $" + aggSubtotal + "<br>";
                                                    aggTotal = aggSubtotal + aggComision;
                                                } else if (origen == "Euro" && destino == "CAN"){
                                                        //Conversion de euro a dolares canadienses
                                                        let muletilla = cantidad / dllAeuro;
                                                        aggSubtotal = muletilla * dllAcan;
                                                        aggComision = aggSubtotal * 0.30;
                                            
                                                        imprimir.innerHTML = imprimir.innerHTML + cantidad + " Euros a Dolares Canadienses: $" + aggSubtotal + "<br>";
                                                        aggTotal = aggSubtotal + aggComision;
                                                    } else if (origen == 0 || destino == 0){
                                                            window.alert('Falto seleccionar alguna moneda');
                                                            }

     } else {
        window.alert('Ingrese la cantidad que desea cambiar. (0 no es una cantidad valida)');
     }


    // subTotal = subTotal + aggSubtotal;
    // comision = comision + aggComision;
    total = total + aggTotal;


        //imprimir subtotal, comision y total
        lblsubtotal.innerHTML = "Subtotal: $" + aggSubtotal;
        lblcomision.innerHTML = "Total de Comision: $" + aggComision;
        lbltotal.innerHTML = "Total a Pagar: $" + total;

}

function Registro(){


    let lblRegistrarResultados = document.getElementById('registrarResultados');
    let lblTotalDeTotales = document.getElementById('totalDeTotales');


    totalDeTotales = totalDeTotales + total;
    contadorDeRegistros++;

    lblRegistrarResultados.innerHTML = lblRegistrarResultados.innerHTML + "Registro No." + contadorDeRegistros + " Total: $" + total + "<br>";
    lblTotalDeTotales.innerHTML = "<hr>" + "Total General: $" + totalDeTotales;


}

function borrarRegistros(){

    let lblsubtotal = document.getElementById('subTotal');
    let lblcomision = document.getElementById('totalComision');
    let lbltotal = document.getElementById('totalPagar');
    let imprimir = document.getElementById("resul");
    
    document.getElementById("cantidad").value = '';
    document.getElementById("monedaOrigen").value = 0;
    document.getElementById("monedaDestino").value = 0;

    lblsubtotal.innerHTML = "Subtotal:";
    lblcomision.innerHTML = "Total de Comision:";
    lbltotal.innerHTML = "Total a Pagar:";
    imprimir.innerHTML = "";

    total = 0;
}


